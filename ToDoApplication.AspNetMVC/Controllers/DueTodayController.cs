﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using ToDoApplication.Domain.Repository;
using ToDoApplication.Domain.Entity;
using ToDoApplication.AspNetMVC.Models;
using System.Diagnostics;

namespace ToDoApplication.AspNetMVC.Controllers
{
    public class DueTodayController : Controller
    {
        private readonly IItemRepository _repository;

        public DueTodayController(IItemRepository rep)
        {
            _repository = rep;
        }

        public IActionResult Index()
        {
            var viewModel = new ItemViewModel
            {
                Items = (List<Item>)_repository.GetItemsDueToday()
            };

            return View(viewModel);
        }


    }


}
