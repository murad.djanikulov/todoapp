﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Linq;
using ToDoApplication.Domain.Repository;
using ToDoApplication.AspNetMVC.Models;
using ToDoApplication.Domain.Entity;
using System.Diagnostics;

namespace ToDoApplication.AspNetMVC.Controllers
{
    public class FieldController : Controller
    {
        private readonly IFieldRepository _repository;
        private readonly IItemRepository _itemRepository;

        public FieldController(IFieldRepository rep, IItemRepository itemRep)
        {
            _repository = rep;
            _itemRepository = itemRep;
        }

        public IActionResult Index(int itemId, int toDoId)
        {
            var viewModel = new FieldViewModel
            {
                Fields = (List<Field>)_repository.GetFieldsByItemId(itemId),
                ItemId = itemId,
                ToDoId = toDoId
            };

            return View(viewModel);
        }

        public IActionResult Create(Field field)
        {
            _repository.Create(field);
            Item item = _itemRepository.Get(field.ItemId);
            return RedirectToAction("Index", new { itemId = field.ItemId, toDoId = item.ToDoId });
        }

        public JsonResult PopulateForm(int id)
        {
            var field = _repository.Get(id);

            return Json(field);
        }

        public IActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Update(Field field)
        {
            _repository.Update(field);
            Item item = _itemRepository.Get(field.ItemId);

            return RedirectToAction("Index", new { itemId = field.ItemId, toDoId = item.ToDoId });
        }


    }
}

