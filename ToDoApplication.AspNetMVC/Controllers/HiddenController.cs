﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ToDoApplication.AspNetMVC.Models;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.AspNetMVC.Controllers
{
    public class HiddenController : Controller
    {
        private readonly IToDoRepository _repository;
        public HiddenController(IToDoRepository repository)
        {
            _repository = repository;
        }

        public IActionResult Index()
        {
            var viewModel = new TodoViewModel
            {
                ToDos = (List<ToDo>)_repository.Hidden(),
            };

            return View(viewModel);
        }

        public IActionResult Unhide(int id)
        {
            _repository.Unhide(id);
            return RedirectToAction("Index");
        }


    }
}
