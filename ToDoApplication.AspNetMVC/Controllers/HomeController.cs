﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ToDoApplication.AspNetMVC.Models;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.AspNetMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly IToDoRepository _repository;
        private readonly IItemRepository _itemRepository;
        private readonly IFieldRepository _fieldRepository;

        public HomeController(IToDoRepository repository, IItemRepository itemRepository, IFieldRepository fieldRepository)
        {
            _repository = repository;
            _itemRepository = itemRepository;
            _fieldRepository = fieldRepository;
        }

        public IActionResult Index()
        {
            var viewModel = new TodoViewModel
            {
                ToDos = (List<ToDo>)_repository.GetAll(),
                ItemCount = _itemRepository.CountItemDueToday(),
            };

            return View(viewModel);
        }

        public JsonResult PopulateForm(int id)
        {
            return Json(_repository.Get(id));
        }

        internal TodoViewModel GetAll()
        {
            List<ToDo> list = (List<ToDo>)_repository.GetAll();

            return new TodoViewModel
            {
                ToDos = list
            };
        }

        public IActionResult Create(ToDo toDo)
        {
            _repository.Create(toDo);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            _repository.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Hide(int id)
        {
            _repository.Hide(id);
            return RedirectToAction("Index");
        }

        public IActionResult Update(ToDo toDo)
        {
            _repository.Update(toDo);
            return RedirectToAction("Index");
        }

        public JsonResult Copy(int toDoId)
        {
            int[] ints = _repository.Copy(toDoId);
            int[] ints1 = _itemRepository.Copy(ints[0], ints[1]);
            int[] ints2 = _fieldRepository.Copy(ints1[0], ints1[1]);
            return Json(ints2);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
