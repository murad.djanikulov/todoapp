﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Linq;
using ToDoApplication.Domain.Repository;
using ToDoApplication.AspNetMVC.Models;
using ToDoApplication.Domain.Entity;
using System.Diagnostics;

namespace ToDoApplication.AspNetMVC.Controllers
{
    public class ItemController : Controller
    {

        private readonly IItemRepository _repository;

        public ItemController(IItemRepository rep)
        {
            _repository = rep;
        }

        public IActionResult Index(int toDoId)
        {
            var viewModel = new ItemViewModel
            {
                Items = (List<Item>)_repository.GetItemsByToDoId(toDoId),
                ToDoId = toDoId
            };

            return View(viewModel);
        }


        public JsonResult Get(int toDoId)
        {
            var items = _repository.GetItemsByToDoId(toDoId);

            return Json(items);
        }

        public IActionResult Create(Item item)
        {
            _repository.Create(item);

            return RedirectToAction("Index", new { toDoId = item.ToDoId });
        }

        public JsonResult PopulateForm(int id)
        {
            var item = _repository.Get(id);

            return Json(item);
        }
        public IActionResult Delete(int id)
        {
            _repository.Delete(id);

            return RedirectToAction("Index");
        }
        public IActionResult Update(Item item)
        {
            _repository.Update(item);

            return RedirectToAction("Index", new { toDoId = item.ToDoId });
        }

    }
}
