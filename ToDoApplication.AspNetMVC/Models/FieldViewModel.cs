using System.Collections.Generic;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.AspNetMVC.Models
{
    public class FieldViewModel
    {
        public List<Field> Fields { get; set; }
        public Field Field { get; set; }

        public int? ItemId { get; set; }
        public int? ToDoId { get; set; }

    }
}