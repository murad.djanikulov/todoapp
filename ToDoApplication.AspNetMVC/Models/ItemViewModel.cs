using System.Collections.Generic;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.AspNetMVC.Models
{
    public class ItemViewModel
    {
        public List<Item> Items { get; set; }
        public Item Item { get; set; }
        //        public ItemStatus Status { get; set; }

        public int? ToDoId { get; set; }

    }
}