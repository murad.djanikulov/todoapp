using System.Collections.Generic;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.AspNetMVC.Models
{
    public class TodoViewModel
    {
        public List<ToDo> ToDos { get; set; }
        public ToDo ToDo { get; set; }

        public int ItemCount { get; set; }
    }
}