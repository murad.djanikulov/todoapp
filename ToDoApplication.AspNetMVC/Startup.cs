using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.AspNetMVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<ToDoDbContext>(opts =>
            {
                opts.UseSqlServer(
                    Configuration["ConnectionStrings:ToDoDBConnection"]);
            });
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IFieldRepository, FieldRepository>();
            services.AddScoped<IToDoRepository, ToDoRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            {
                app.UseExceptionHandler("/Home/Error");

                app.UseStaticFiles(); //wwwroot

                app.UseRouting(); // determine which controller and action should handle an incoming request based on the URL.

                app.UseAuthorization();

                app.UseEndpoints(endpoints => // defines the endpoints and their corresponding routes for the application
                {
                    endpoints.MapControllerRoute( // map routes to specific controllers and actions.
                        name: "default",
                        pattern: "{controller=Home}/{action=Index}/{id?}");

                    endpoints.MapControllerRoute(
                        name: "item",
                        pattern: "{controller=Item}/{action=Index}/{id?}");

                    endpoints.MapControllerRoute(
                        name: "field",
                        pattern: "{controller=Field}/{action=Index}/{id?}");

                    endpoints.MapControllerRoute(
                        name: "duetoday",
                        pattern: "{controller=DueToday}/{action=Index}/{id?}");

                    endpoints.MapControllerRoute(
                        name: "hidden",
                        pattern: "{controller=Hidden}/{action=Index}/{id?}");

                });
            }
        }
    }
}
