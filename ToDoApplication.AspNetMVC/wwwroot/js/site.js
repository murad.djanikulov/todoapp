﻿function deleteToDo(i) {
    $.ajax({
        url: 'Home/Delete',
        type: 'POST',
        data: {
            id: i
        },
        success: function () {
            window.location.reload();
        }
    });
}

function hideToDo(i) {
    $.ajax({
        url: 'Home/Hide',
        type: 'POST',
        data: {
            id: i
        },
        success: function () {
            window.location.reload();
        }
    });
}

function unhideToDo(i) {
    $.ajax({
        url: 'Hidden/Unhide',
        type: 'POST',
        data: {
            id: i
        },
        success: function () {
            window.location.reload();
        }
    });
}

function populateFormToDo(i) {

    $.ajax({
        url: 'Home/PopulateForm',
        type: 'GET',
        data: {
            id: i
        },
        dataType: 'json',
        success: function (response) {
            $("#form1-container #ToDo_Name").val(response.name);
            $("#form1-container #ToDo_Id").val(response.id);
            $("#form1-container #form-button").val("Update ToDo");
            $("#form1-container #form-action").attr("action", "/Home/Update");
        }
    });
}

function deleteItem(i) {
    $.ajax({
        url: 'Item/Delete',
        type: 'POST',
        data: {
            id: i
        },
        success: function () {
            window.location.reload();
        }
    });
}

function populateFormItem(i, status) {

    $.ajax({
        url: 'Item/PopulateForm',
        type: 'GET',
        data: {
            id: i
        },
        dataType: 'json',
        success: function (response) {
            $("#form2-container #Item_Id").val(response.id);
            $("#form2-container #Item_Description").val(response.description);
            $("#form2-container #Item_ToDoId").val(response.toDoId);
            $("#form2-container #Item_Status").val(status);
            let dueDate = new Date(response.dueDate);
            let formattedDueDate = new Date(dueDate.getTime() - dueDate.getTimezoneOffset() * 60000).toISOString().split("T")[0];
            $("#form2-container #Item_DueDate").val(formattedDueDate);
            $("#form2-container #form-button").val("Update Item");
            $("#form2-container #form-action").attr("action", "/Item/Update");
        }
    });
}

function toggleCompletedItems() {
    let toggleCheckbox = document.getElementById("toggleCompleted");
    let rows = document.getElementsByClassName("completed");

    for (const element of rows) {
        element.style.display = toggleCheckbox.checked ? "table-row" : "none";
    }
}



function copyToDo(toDoId) {
    $.ajax({
        url: 'Home/Copy',
        type: 'POST',
        data: { toDoId: toDoId },
        success: function (response) {
            window.location.reload();
        }
    });
}


function deleteField(i) {
    $.ajax({
        url: 'Field/Delete',
        type: 'POST',
        data: {
            id: i
        },
        success: function () {
            window.location.reload();
        }
    });
}
function populateFormField(i) {

    $.ajax({
        url: 'Field/PopulateForm',
        type: 'GET',
        data: {
            id: i
        },
        dataType: 'json',
        success: function (response) {
            $("#form2-container #Field_Id").val(response.id);
            $("#form2-container #Field_Name").val(response.name);
            $("#form2-container #Field_Value").val(response.value);
            $("#form2-container #Field_ItemId").val(response.itemId);
            $("#form2-container #form-button").val("Update Field");
            $("#form2-container #form-action").attr("action", "/Field/Update");
        }
    });
}
