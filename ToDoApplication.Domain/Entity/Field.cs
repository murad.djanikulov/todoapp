﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoApplication.Domain.Entity
{
    public class Field
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public int ItemId { get; set; }
    }

}
