﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoApplication.Domain.Entity
{
    public class ToDo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsHidden { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}
