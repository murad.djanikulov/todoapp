﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public class FieldRepository : IFieldRepository
    {
        private readonly ToDoDbContext context;
        private readonly IItemRepository itemRepository;

        public FieldRepository(ToDoDbContext ctx, IItemRepository rep)
        {
            context = ctx;
            itemRepository = rep;
        }
        public Field Create(Field field)
        {
            Item item = itemRepository.Get(field.ItemId);
            if (item != null)
            {
                context.Fields.Add(field);
                context.SaveChanges();
                return field;
            }
            return null;
        }

        public bool Delete(int id)
        {
            var field = context.Fields.Find(id);
            if (field != null)
            {
                context.Fields.Remove(field);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Field Get(int id)
        {
            return context.Fields.Find(id);
        }

        public IEnumerable<Field> GetAll()
        {
            return context.Fields.ToList();
        }

        public IEnumerable<Field> GetFieldsByItemId(int itemId)
        {
            return context.Fields.Where(field => field.ItemId == itemId).ToList();
        }

        public Field Update(Field field)
        {
            var updatedField = context.Fields.FirstOrDefault(t => t.Equals(field));
            if (updatedField != null)
            {
                updatedField.Name = field.Name;
                updatedField.Value = field.Value;
                context.Update(updatedField);
                context.SaveChanges();
                return field;
            }
            return null;
        }
        public int[] Copy(int existedToDoId, int copiedToDoId)
        {
            IEnumerable<Item> existedItems = itemRepository.GetItemsByToDoId(existedToDoId);

            IEnumerable<Item> copiedItems = itemRepository.GetItemsByToDoId(copiedToDoId);

            Field copiedField = new Field();
            List<Field> existedFields;
            List<Field> copiedFields = new List<Field>();

            foreach (Item existedItem in existedItems)
            {
                foreach (Item copiedItem in copiedItems)
                {
                    if (existedItem.Description.Equals(copiedItem.Description)
                        && existedItem.Status.Equals(copiedItem.Status)
                        && existedItem.DueDate.Equals(copiedItem.DueDate))
                    {
                        existedFields = GetFieldsByItemId(existedItem.Id).ToList();
                        foreach (Field field in existedFields)
                        {
                            copiedField = new Field
                            {
                                Name = field.Name,
                                Value = field.Value,
                                ItemId = copiedItem.Id,
                            };
                            copiedField = Create(copiedField);
                            copiedFields.Add(copiedField);
                        }

                        //                        List<Field> copiedFieldss = GetFieldsByItemId(copiedField.ItemId).ToList();
                        //                        copiedItem.Fields = copiedFieldss;
                        //                        itemRepository.Update(copiedItem);

                    }
                }
            }

            return new int[] { existedToDoId, copiedToDoId };
        }
    }
}
