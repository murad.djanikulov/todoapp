﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public interface IFieldRepository : IRepository<Field>
    {
        IEnumerable<Field> GetFieldsByItemId(int itemId);

        int[] Copy(int existedToDoId, int copiedToDoId);
    }
}
