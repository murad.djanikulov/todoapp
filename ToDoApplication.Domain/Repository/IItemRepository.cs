﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public interface IItemRepository : IRepository<Item>
    {
        IEnumerable<Item> GetItemsByToDoId(int toDoId);

        int[] Copy(int existedToDoId, int copiedToDoId);

        IEnumerable<Item> GetItemsDueToday();

        int CountItemDueToday();



    }
}
