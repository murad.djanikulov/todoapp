﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public interface IToDoRepository : IRepository<ToDo>
    {
        bool Hide(int id);
        bool Unhide(int id);

        int[] Copy(int id);
        IEnumerable<ToDo> Hidden();


    }
}
