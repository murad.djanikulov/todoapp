﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public class ItemRepository : IItemRepository
    {
        private readonly ToDoDbContext context;
        private readonly IToDoRepository toDoRepository;

        public ItemRepository(ToDoDbContext ctx, IToDoRepository rep)
        {
            context = ctx;
            toDoRepository = rep;
        }

        public Item Create(Item item)
        {
            ToDo toDo = toDoRepository.Get(item.ToDoId);
            if (toDo != null)
            {
                item.CreationDate = DateTime.Now;
                context.Items.Add(item);
                context.SaveChanges();
                return item;
            }
            return null;
        }

        public bool Delete(int id)
        {
            var item = context.Items.Find(id);
            if (item != null)
            {
                context.Items.Remove(item);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Item Get(int id)
        {
            return context.Items.Find(id);
        }

        public IEnumerable<Item> GetAll()
        {
            return context.Items.ToList();
        }

        public IEnumerable<Item> GetItemsByToDoId(int toDoId)
        {
            return context.Items.Where(item => item.ToDoId == toDoId).ToList();
        }

        public Item Update(Item item)
        {
            var updatedItem = context.Items.FirstOrDefault(t => t.Equals(item));
            if (updatedItem != null)
            {
                updatedItem.Description = item.Description;
                updatedItem.Status = item.Status;
                updatedItem.DueDate = item.DueDate;
                context.Update(updatedItem);
                context.SaveChanges();
                return item;
            }
            return null;
        }
        public int[] Copy(int existedToDoId, int copiedToDoId)
        {
            IEnumerable<Item> todoItems = GetItemsByToDoId(existedToDoId);

            foreach (Item item in todoItems)
            {
                Item newItem = new Item
                {
                    Description = item.Description,
                    ToDoId = copiedToDoId,
                    Status = item.Status,
                    DueDate = item.DueDate
                };
                Create(newItem);

            }

            return new int[] { existedToDoId, copiedToDoId };
        }

        public IEnumerable<Item> GetItemsDueToday()
        {
            List<Item> items = context.Items.Where(t => t.DueDate.DayOfYear.Equals(DateTime.Today.DayOfYear)).ToList();
            List<int> toDoIds = toDoRepository.GetAll().Select(toDo => toDo.Id).ToList();
            items = items.Where(item => toDoIds.Contains(item.ToDoId)).ToList();
            return items;


        }

        public int CountItemDueToday()
        {
            int today = GetItemsDueToday().Count();
            return today;
        }
    }
}
