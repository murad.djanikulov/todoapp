﻿using Microsoft.EntityFrameworkCore;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public class ToDoDbContext : DbContext
    {
        public ToDoDbContext(DbContextOptions<ToDoDbContext> options) : base(options) { }
        public DbSet<ToDo> ToDos { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Field> Fields { get; set; }

        /*        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
                {
                    optionsBuilder.UseSqlServer(
                        @"Server = .\SQLEXPRESS; database = ToDoDB; Integrated Security = true; trustServerCertificate = true");

                }
        */
    }

}
