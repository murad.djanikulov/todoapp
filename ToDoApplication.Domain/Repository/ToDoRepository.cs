﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public class ToDoRepository : IToDoRepository
    {
        private readonly ToDoDbContext context;

        public ToDoRepository(ToDoDbContext ctx)
        {
            context = ctx;
        }

        public int[] Copy(int id)
        {
            ToDo existedToDo = Get(id);
            ToDo copiedToDo = new ToDo();
            if (existedToDo != null)
            {
                copiedToDo = new ToDo { Name = existedToDo.Name, Items = existedToDo.Items };
                copiedToDo = Create(copiedToDo);

            }
            return new int[] { id, copiedToDo.Id };
        }

        public ToDo Create(ToDo toDo)
        {
            toDo.IsHidden = false;
            context.ToDos.Add(toDo);
            context.SaveChanges();
            return toDo;
        }

        public bool Delete(int id)
        {
            var toDo = context.ToDos.Find(id);
            if (toDo != null)
            {
                context.ToDos.Remove(toDo);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public ToDo Get(int id)
        {
            return context.ToDos.Find(id);
        }

        public IEnumerable<ToDo> GetAll()
        {
            return context.ToDos.Where(toDo => !toDo.IsHidden).OrderBy(toDo => toDo.Id).ToList();
        }

        public IEnumerable<ToDo> Hidden()
        {
            return context.ToDos.Where(toDo => toDo.IsHidden).OrderBy(toDo => toDo.Id).ToList();
        }

        public bool Hide(int id)
        {
            var todo = context.ToDos.Find(id);
            if (todo != null)
            {
                todo.IsHidden = true;
                context.Update(todo);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Unhide(int id)
        {
            var todo = context.ToDos.Find(id);
            if (todo != null)
            {
                todo.IsHidden = false;
                context.Update(todo);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public ToDo Update(ToDo toDo)
        {
            var updatedToDo = context.ToDos.FirstOrDefault(t => t.Equals(toDo));
            if (updatedToDo != null)
            {
                updatedToDo.Name = toDo.Name;
                context.Update(updatedToDo);
                context.SaveChanges();
                return toDo;
            }
            return null;
        }
    }
}
