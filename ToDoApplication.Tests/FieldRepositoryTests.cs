﻿using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.Tests
{
    [TestFixture]
    public class FieldRepositoryTests
    {
        private ToDoDbContext _context;
        private IItemRepository _itemRepository;
        private IFieldRepository _fieldRepository;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ToDoDbContext>()
                .UseSqlServer(@"Server=DESKTOP-MSFTMDM\\SQLEXPRESS;database=ToDoDB;Integrated Security=true;trustServerCertificate=true;")
                .Options;
            _context = new ToDoDbContext(options);

            _itemRepository = new ItemRepository(_context, new ToDoRepository(_context));
            _fieldRepository = new FieldRepository(_context, _itemRepository);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Create_ValidField_ShouldAddFieldToContext()
        {
            var toDo = new ToDo { Name = "NewToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();
            var item = new Item { ToDoId = toDo.Id };
            _context.Items.Add(item);
            _context.SaveChanges();

            var field = new Field { Name = "Field1", Value = "Value1", ItemId = item.Id };

            var result = _fieldRepository.Create(field);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Name, Is.EqualTo(field.Name));
            Assert.That(result.Value, Is.EqualTo(field.Value));
            Assert.That(result.ItemId, Is.EqualTo(field.ItemId));
            Assert.That(result.Id, Is.Not.EqualTo(0));

            var addedField = _context.Fields.Find(result.Id);
            Assert.That(addedField, Is.Not.Null);
            Assert.That(addedField.Name, Is.EqualTo(field.Name));
            Assert.That(addedField.Value, Is.EqualTo(field.Value));
            Assert.That(addedField.ItemId, Is.EqualTo(field.ItemId));
        }

        [Test]
        public void Create_InvalidItemId_ShouldNotAddFieldToContext()
        {
            var field = new Field { ItemId = 9999 };

            var result = _fieldRepository.Create(field);

            Assert.That(result, Is.Null);

            var addedField = _context.Fields.FirstOrDefault(f => f.Name == field.Name && f.Value == field.Value);
            Assert.That(addedField, Is.Null);

        }

        [Test]
        public void Delete_ExistingFieldId_ShouldRemoveFieldFromContext()
        {
            var toDo = new ToDo { Name = "NewToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item = new Item { ToDoId = toDo.Id };
            _context.Items.Add(item);
            _context.SaveChanges();

            var field = new Field { Name = "Field1", Value = "Value1", ItemId = item.Id };
            _context.Fields.Add(field);
            _context.SaveChanges();

            var result = _fieldRepository.Delete(field.Id);

            Assert.That(result, Is.True);

            var deletedField = _context.Fields.Find(field.Id);
            Assert.That(deletedField, Is.Null);
        }

        [Test]
        public void Delete_NonExistingFieldId_ShouldNotRemoveFieldFromContext()
        {
            var nonExistingFieldId = 9999;

            var result = _fieldRepository.Delete(nonExistingFieldId);

            Assert.That(result, Is.False);
        }

        [Test]
        public void Get_ExistingFieldId_ShouldReturnFieldFromContext()
        {
            var toDo = new ToDo { Name = "NewToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item = new Item { ToDoId = toDo.Id };
            _context.Items.Add(item);
            _context.SaveChanges();

            var field = new Field { Name = "Field1", Value = "Value1", ItemId = item.Id };
            _context.Fields.Add(field);
            _context.SaveChanges();

            var result = _fieldRepository.Get(field.Id);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(field.Id));
            Assert.That(result.Name, Is.EqualTo(field.Name));
            Assert.That(result.Value, Is.EqualTo(field.Value));
        }

        [Test]
        public void Get_NonExistingFieldId_ShouldReturnNull()
        {
            var nonExistingFieldId = 9999;

            var result = _fieldRepository.Get(nonExistingFieldId);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetAll_ShouldReturnAllFieldsFromContext()
        {
            var toDo = new ToDo { Name = "NewToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item = new Item { ToDoId = toDo.Id };
            _context.Items.Add(item);
            _context.SaveChanges();

            var field1 = new Field { Name = "Field1", Value = "Value1", ItemId = item.Id };
            var field2 = new Field { Name = "Field2", Value = "Value2", ItemId = item.Id };
            _context.Fields.AddRange(field1, field2);
            _context.SaveChanges();

            var result = _fieldRepository.GetAll();
            result = result.Where(field => field2.Equals(field) || field1.Equals(field)).ToList();


            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result, Contains.Item(field1));
            Assert.That(result, Contains.Item(field2));
        }

        [Test]
        public void GetFieldsByItemId_ExistingItemId_ShouldReturnFieldsWithMatchingItemId()
        {
            var toDo = new ToDo { Name = "NewToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item1 = new Item { ToDoId = toDo.Id };
            var item2 = new Item { ToDoId = toDo.Id };
            _context.Items.AddRange(item1, item2);
            _context.SaveChanges();

            var field1 = new Field { Name = "Field1", Value = "Value1", ItemId = item1.Id };
            var field2 = new Field { Name = "Field2", Value = "Value2", ItemId = item1.Id };
            var field3 = new Field { Name = "Field3", Value = "Value3", ItemId = item2.Id };
            var field4 = new Field { Name = "Field4", Value = "Value4", ItemId = item2.Id };
            _context.Fields.AddRange(field1, field2, field3, field4);
            _context.SaveChanges();

            var result = _fieldRepository.GetFieldsByItemId(item1.Id);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result, Contains.Item(field1));
            Assert.That(result, Contains.Item(field2));
            Assert.That(result, Does.Not.Contain(field3));
        }

        [Test]
        public void GetFieldsByItemId_NonExistingItemId_ShouldReturnEmptyList()
        {
            var nonExistingItemId = 9999;

            var result = _fieldRepository.GetFieldsByItemId(nonExistingItemId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Empty);
        }

        [Test]
        public void Update_ExistingField_ShouldUpdateFieldInContext()
        {
            var toDo = new ToDo { Name = "NewToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item = new Item { ToDoId = toDo.Id };
            _context.Items.Add(item);
            _context.SaveChanges();

            var field = new Field { Name = "Field1", Value = "Value1", ItemId = item.Id };

            _context.Fields.Add(field);
            _context.SaveChanges();

            var updatedField = new Field { Id = field.Id, Name = "UpdatedField", Value = "UpdatedValue" };

            var result = _fieldRepository.Update(updatedField);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Id, Is.EqualTo(updatedField.Id));
            Assert.That(result.Name, Is.EqualTo(updatedField.Name));
            Assert.That(result.Value, Is.EqualTo(updatedField.Value));

            var updatedFieldInContext = _context.Fields.Find(field.Id);
            Assert.That(updatedFieldInContext, Is.Not.Null);
            Assert.That(updatedFieldInContext.Name, Is.EqualTo(updatedField.Name));
            Assert.That(updatedFieldInContext.Value, Is.EqualTo(updatedField.Value));
        }

        [Test]
        public void Update_NonExistingField_ShouldNotUpdateFieldInContext()
        {
            var nonExistingField = new Field { Id = 9999, Name = "Field1", Value = "Value1" };

            var result = _fieldRepository.Update(nonExistingField);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void Copy_ValidToDoIds_ShouldCopyFieldsToDestinationToDo()
        {
            var toDo1 = new ToDo { Name = "ToDo1" };
            var toDo2 = new ToDo { Name = "ToDo2" };
            _context.ToDos.AddRange(toDo1, toDo2);
            _context.SaveChanges();

            var existedToDoId = toDo1.Id;
            var copiedToDoId = toDo2.Id;

            var item1 = new Item { Description = "Item", Status = ItemStatus.InProgress, DueDate = DateTime.Today, ToDoId = existedToDoId };
            var item2 = new Item { Description = "Item", Status = ItemStatus.InProgress, DueDate = DateTime.Today, ToDoId = copiedToDoId };
            _context.Items.AddRange(item1, item2);
            _context.SaveChanges();

            var field1 = new Field { Name = "Field", Value = "Value", ItemId = item1.Id };
            _context.Fields.Add(field1);
            _context.SaveChanges();

            var result = _fieldRepository.Copy(existedToDoId, copiedToDoId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Length, Is.EqualTo(2));
            Assert.That(result[0], Is.EqualTo(existedToDoId));
            Assert.That(result[1], Is.EqualTo(copiedToDoId));

            List<Field> existedFields = _fieldRepository.GetFieldsByItemId(item1.Id).ToList();
            List<Field> copiedFields = _fieldRepository.GetFieldsByItemId(item2.Id).ToList();
            Assert.That(copiedFields, Is.Not.Null);
            Assert.That(copiedFields.Count(), Is.EqualTo(1));
            Assert.That(copiedFields[0].Name, Is.EqualTo(existedFields[0].Name));
            Assert.That(copiedFields[0].Value, Is.EqualTo(existedFields[0].Value));
        }
    }

}
