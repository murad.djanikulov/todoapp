using NUnit.Framework;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;
using NUnit.Framework.Interfaces;

namespace ToDoApplication.Tests
{
    [TestFixture]
    public class ItemRepositoryTests
    {
        private ToDoDbContext _context;
        private IItemRepository _itemRepository;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ToDoDbContext>()
                .UseSqlServer(@"Server=DESKTOP-MSFTMDM\\SQLEXPRESS;database=ToDoDB;Integrated Security=true;trustServerCertificate=true;")
                .Options;
            _context = new ToDoDbContext(options);

            _itemRepository = new ItemRepository(_context, new ToDoRepository(_context));
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Create_ValidItem_ShouldAddItemToContext()
        {
            var toDo = new ToDo { Name = "Test ToDo" };

            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item = new Item { ToDoId = toDo.Id };

            var result = _itemRepository.Create(item);

            Assert.That(result, Is.Not.Null);
            Assert.That(_context.Items.Contains(item), Is.True);
        }

        [Test]
        public void Create_InvalidToDoId_ShouldNotAddItemToContext()
        {
            var invalidToDoId = 9999;
            var item = new Item { ToDoId = invalidToDoId };

            var result = _itemRepository.Create(item);

            Assert.That(result, Is.Null);
            Assert.That(_context.Items.Contains(item), Is.False);
        }

        [Test]
        public void Delete_ExistingId_ShouldRemoveItemFromContext()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var existingItem = new Item();

            existingItem = new Item { ToDoId = toDo.Id };
            _context.Items.Add(existingItem);
            _context.SaveChanges();

            var result = _itemRepository.Delete(existingItem.Id);

            Assert.That(result, Is.True);
            Assert.That(_context.Items.Contains(existingItem), Is.False);
        }

        [Test]
        public void Delete_NonExistingId_ShouldReturnFalse()
        {
            var nonExistingId = 9999;

            var result = _itemRepository.Delete(nonExistingId);

            Assert.IsFalse(result);
        }

        [Test]
        public void Get_ExistingId_ShouldReturnItem()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var existingItem = new Item { ToDoId = toDo.Id };
            _context.Items.Add(existingItem);
            _context.SaveChanges();

            var result = _itemRepository.Get(existingItem.Id);

            Assert.That(result, Is.EqualTo(existingItem));
        }

        [Test]
        public void Get_NonExistingId_ShouldReturnNull()
        {
            var nonExistingId = 9999;

            var result = _itemRepository.Get(nonExistingId);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetAll_ShouldReturnAllItems()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var items = new List<Item>
        {
            new Item { ToDoId = toDo.Id },
            new Item { ToDoId = toDo.Id },
            new Item { ToDoId = toDo.Id }
        };
            _context.Items.AddRange(items);
            _context.SaveChanges();

            var result = _itemRepository.GetAll();
            result = result.Where(item => items.Contains(item) || toDo.Id.Equals(item.ToDoId)).ToList();


            Assert.That(result.Count(), Is.EqualTo(items.Count));
            CollectionAssert.AreEquivalent(items, result);
        }

        [Test]
        public void Update_ExistingItem_ShouldUpdateItemAndSaveChanges()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var existingItem = new Item { Description = "ExistingItem", Status = ItemStatus.InProgress, ToDoId = toDo.Id };
            _context.Items.Add(existingItem);
            _context.SaveChanges();

            var updatedItem = new Item { Id = existingItem.Id, Description = "UpdatedItem", Status = ItemStatus.Completed, ToDoId = toDo.Id };

            var result = _itemRepository.Update(updatedItem);

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.EqualTo(updatedItem));

        }

        [Test]
        public void Update_NonExistingItem_ShouldReturnNull()
        {

            var nonExistingItem = new Item { Id = 9999, Description = "NonExisting Item", Status = ItemStatus.InProgress };

            var result = _itemRepository.Update(nonExistingItem);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void Copy_ValidExistedToDoIdAndCopiedToDoId_ShouldCopyItemsToCopiedToDo()
        {
            var existedToDo = new ToDo { Name = "ExistedToDo" };
            var copiedToDo = new ToDo { Name = "CopiedToDo" };
            _context.ToDos.Add(existedToDo);
            _context.ToDos.Add(copiedToDo);
            _context.SaveChanges();

            var items = new List<Item>
        {
            new Item { Description = "Item1", ToDoId = existedToDo.Id },
            new Item { Description = "Item2", ToDoId = existedToDo.Id },
            new Item { Description = "Item3", ToDoId = existedToDo.Id }
        };
            _context.Items.AddRange(items);
            _context.SaveChanges();

            var result = _itemRepository.Copy(existedToDo.Id, copiedToDo.Id);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Length, Is.EqualTo(2));
            Assert.That(result[0], Is.EqualTo(existedToDo.Id));
            Assert.That(result[1], Is.EqualTo(copiedToDo.Id));

            var copiedItems = _context.Items.Where(item => item.ToDoId == copiedToDo.Id).ToList();
            Assert.That(copiedItems.Count, Is.EqualTo(items.Count));
            CollectionAssert.AreEquivalent(items.Select(item => item.Description), copiedItems.Select(item => item.Description));
        }

        [Test]
        public void GetItemsByToDoId_ExistingToDoId_ShouldReturnItemsWithMatchingToDoId()
        {
            var toDo = new ToDo { Name = "Test ToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var items = new List<Item>
        {
            new Item { Description = "Item1", ToDoId = toDo.Id },
            new Item { Description = "Item2", ToDoId = toDo.Id },
            new Item { Description = "Item3", ToDoId = toDo.Id }
        };
            _context.Items.AddRange(items);
            _context.SaveChanges();

            var result = _itemRepository.GetItemsByToDoId(toDo.Id);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.EqualTo(items.Count));
            CollectionAssert.AreEquivalent(items, result);
        }

        [Test]
        public void GetItemsDueToday_ShouldReturnItemsDueTodayForVisibleToDos()
        {
            var visibleToDo = new ToDo { Name = "VisibleToDo", IsHidden = false };
            var hiddenToDo = new ToDo { Name = "HiddenToDo", IsHidden = true };
            _context.ToDos.Add(visibleToDo);
            _context.ToDos.Add(hiddenToDo);
            _context.SaveChanges();

            var today = DateTime.Today;
            var itemsDueToday = new List<Item>
        {
            new Item { Description = "Item1", ToDoId = visibleToDo.Id, DueDate = today },
            new Item { Description = "Item2", ToDoId = hiddenToDo.Id, DueDate = today },
            new Item { Description = "Item3", ToDoId = visibleToDo.Id, DueDate = today.AddDays(1) }
        };
            _context.Items.AddRange(itemsDueToday);
            _context.SaveChanges();

            var result = _itemRepository.GetItemsDueToday();
            result = result.Where(item => itemsDueToday.Contains(item)).ToList();



            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.EqualTo(1));
            Assert.That(result.First().Description, Is.EqualTo("Item1"));
            Assert.That(result.First().ToDoId, Is.EqualTo(visibleToDo.Id));
            Assert.That(result.First().DueDate, Is.EqualTo(today));
        }

        [Test]
        public void CountItemDueToday_ShouldReturnNumberOfItemsDueTodayForVisibleToDos()
        {
            _context.ToDos.RemoveRange(_context.ToDos.ToList());
            _context.SaveChanges();
            var visibleToDo = new ToDo { Name = "VisibleToDo", IsHidden = false };
            var hiddenToDo = new ToDo { Name = "HiddenToDo", IsHidden = true };
            _context.ToDos.Add(visibleToDo);
            _context.ToDos.Add(hiddenToDo);
            _context.SaveChanges();

            var today = DateTime.Today;
            var itemsDueToday = new List<Item>
        {
            new Item { Description = "Item1", ToDoId = visibleToDo.Id, DueDate = today },
            new Item { Description = "Item2", ToDoId = hiddenToDo.Id, DueDate = today },
            new Item { Description = "Item3", ToDoId = visibleToDo.Id, DueDate = today.AddDays(1) }
        };
            _context.Items.AddRange(itemsDueToday);
            _context.SaveChanges();

            var result = _itemRepository.CountItemDueToday();

            Assert.That(result, Is.EqualTo(1));
        }
    }
}

