﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.Tests
{


    [TestFixture]
    public class ToDoRepositoryTests
    {
        private ToDoDbContext _context;
        private IToDoRepository _toDoRepository;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ToDoDbContext>()
            .UseSqlServer(@"Server=DESKTOP-MSFTMDM\\SQLEXPRESS;database=ToDoDB;Integrated Security=true;trustServerCertificate=true;")
            .Options;
            _context = new ToDoDbContext(options);

            _toDoRepository = new ToDoRepository(_context);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void CreateToDo_ShouldAddNewToDoToContext()
        {
            var newToDo = new ToDo { Name = "NewToDo" };

            var result = _toDoRepository.Create(newToDo);

            Assert.That(result, Is.EqualTo(newToDo));
            Assert.Contains(newToDo, _context.ToDos.ToList());
        }

        [Test]
        public void DeleteToDo_ExistingId_ShouldRemoveToDoFromContext()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo" };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Delete(existingId);

            Assert.That(result, Is.True);
            Assert.That(_context.ToDos.Contains(existingToDo), Is.False);
        }

        [Test]
        public void DeleteToDo_NonExistingId_ShouldReturnFalse()
        {
            var nonExistingId = 999;

            var result = _toDoRepository.Delete(nonExistingId);

            Assert.That(result, Is.False);
        }

        [Test]
        public void GetToDo_ExistingId_ShouldReturnToDo()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo" };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Get(existingId);

            Assert.That(result, Is.EqualTo(existingToDo));
        }

        [Test]
        public void GetToDo_NonExistingId_ShouldReturnNull()
        {
            var nonExistingId = 9999;

            var result = _toDoRepository.Get(nonExistingId);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetAll_ShouldReturnVisibleToDosOrderedById()
        {
            var visibleToDos = new List<ToDo>
        {
            new ToDo { Name = "ToDo1", IsHidden = false },
            new ToDo { Name = "ToDo2", IsHidden = false },
            new ToDo { Name = "ToDo3", IsHidden = false },
        };
            var hiddenToDo = new ToDo { Name = "HiddenToDo", IsHidden = true };
            _context.ToDos.AddRange(visibleToDos);
            _context.ToDos.Add(hiddenToDo);
            _context.SaveChanges();

            var result = _toDoRepository.GetAll();
            result = result.Where(toDo => visibleToDos.Contains(toDo) || hiddenToDo.Equals(toDo)).ToList();

            Assert.That(result.Count(), Is.EqualTo(visibleToDos.Count));
            Assert.That(result.All(toDo => !toDo.IsHidden), Is.True);
            Assert.That(result.OrderBy(toDo => toDo.Id).SequenceEqual(visibleToDos), Is.True);
        }

        [Test]
        public void Update_ExistingToDo_ShouldUpdateToDoAndSaveChanges()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo", IsHidden = false };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();

            var updatedToDo = new ToDo { Id = existingToDo.Id, Name = "UpdatedToDo", IsHidden = false };

            var result = _toDoRepository.Update(updatedToDo);

            Assert.That(result, Is.EqualTo(updatedToDo));

            var updatedEntity = _context.ToDos.First(t => t.Id == updatedToDo.Id);
            Assert.That(updatedEntity.Name, Is.EqualTo(updatedToDo.Name));
            Assert.That(updatedEntity.IsHidden, Is.False);
        }

        [Test]
        public void Update_NonExistingToDo_ShouldReturnNull()
        {
            var nonExistingToDo = new ToDo { Id = 9999, Name = "NonExistingToDo", IsHidden = false };

            var result = _toDoRepository.Update(nonExistingToDo);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void Copy_ExistingId_ShouldCopyToDoAndReturnArrayWithOriginalAndCopiedIds()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo" };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Copy(existingId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Length, Is.EqualTo(2));
            Assert.That(result[0], Is.EqualTo(existingId));
            Assert.That(result[1], Is.Not.EqualTo(existingId));
        }

        [Test]
        public void Copy_NonExistingId_ShouldReturnArrayWithZeroIds()
        {
            var nonExistingId = 9999;

            var result = _toDoRepository.Copy(nonExistingId);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Length, Is.EqualTo(2));
            Assert.That(result[0], Is.EqualTo(nonExistingId));
            Assert.That(result[1], Is.EqualTo(0));
        }

        [Test]
        public void Hidden_ShouldReturnHiddenToDosOrderedById()
        {
            var hiddenToDos = new List<ToDo>
        {
            new ToDo { Name = "HiddenToDo1", IsHidden = true },
            new ToDo { Name = "HiddenToDo2", IsHidden = true },
            new ToDo { Name = "HiddenToDo3", IsHidden = true }
        };
            var visibleToDo = new ToDo { Name = "VisibleToDo", IsHidden = false };
            _context.ToDos.AddRange(hiddenToDos);
            _context.ToDos.Add(visibleToDo);
            _context.SaveChanges();

            var result = _toDoRepository.Hidden();
            result = result.Where(toDo => hiddenToDos.Contains(toDo) || visibleToDo.Equals(toDo)).ToList();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count(), Is.EqualTo(hiddenToDos.Count));
            Assert.That(result.All(toDo => toDo.IsHidden), Is.True);
            Assert.That(result.OrderBy(toDo => toDo.Id).SequenceEqual(hiddenToDos), Is.True);
        }

        [Test]
        public void Hide_ExistingId_ShouldHideToDoAndSaveChanges()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo", IsHidden = false };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Hide(existingId);

            Assert.That(result, Is.True);

            var hiddenEntity = _context.ToDos.First(t => t.Id == existingId);
            Assert.That(hiddenEntity.IsHidden, Is.True);
        }

        [Test]
        public void Hide_NonExistingId_ShouldReturnFalse()
        {
            var nonExistingId = 9999;

            var result = _toDoRepository.Hide(nonExistingId);

            Assert.That(result, Is.False);
        }

        [Test]
        public void Unhide_ExistingId_ShouldUnhideToDoAndSaveChanges()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo", IsHidden = true };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Unhide(existingId);

            Assert.That(result, Is.True);

            var unhiddenEntity = _context.ToDos.First(t => t.Id == existingId);
            Assert.That(unhiddenEntity.IsHidden, Is.False);
        }

        [Test]
        public void Unhide_NonExistingId_ShouldReturnFalse()
        {
            var nonExistingId = 9999;

            var result = _toDoRepository.Unhide(nonExistingId);

            Assert.That(result, Is.False);
        }
    }

}
